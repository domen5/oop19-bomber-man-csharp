﻿using System;
namespace it.bomberman.hud
{

	public class Clock
	{

		private Timer timer;
		private RemindTask remindTask;

		public Clock(int seconds)
		{
			this.timer = new Timer();
			remindTask = new RemindTask(this, seconds);
			timer.schedule(remindTask, 0, 1 * 1000); // Intervallo esecuzioni ogni sec
		}

		public virtual string Time
		{
			get
			{
				if (remindTask.Time == 0)
				{
					return "000";
				}
				else
				{
					return remindTask.Time + "";
				}
			}
		}

		public class RemindTask : TimerTask
		{
			private readonly Clock outerInstance;

			internal int time;

			public RemindTask(Clock outerInstance, int time)
			{
				this.outerInstance = outerInstance;
				this.time = time;
			}

			public override void run()
			{
				if (time != 0)
				{
					time--;
				}
			}

			public virtual int Time
			{
				get
				{
					return this.time;
				}
			}
		}
	}

}
