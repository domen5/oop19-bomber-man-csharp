﻿using System;

using System.Collections.Generic;

using Entity = it.bomberman.entity.creature.Entity;
using Player = it.bomberman.entity.creature.Player;
using Clock = it.bomberman.hud.Clock;

public interface ArenaModel
{
	void update();

	void register(Entity entity);

	IList<Entity> Drawables { get; }

	Player P1 { get; }

	Player P2 { get; }

	Clock Clock { get; }

	Optional<Player> Winner { get; }

	bool gameOver();
}
