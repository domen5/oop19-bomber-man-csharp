﻿using System;
using System.Collections.Generic;

namespace it.bomberman.entities
{
    using Body = it.bomberman.collisions.Body;
    using ICollidable = it.bomberman.collisions.ICollidable;
    using Rectangle = it.bomberman.collisions.Rectangle;
    using Vector2 = it.bomberman.collisions.Vector2;
    using PowerUpType = it.bomberman.entities.PowerUp.PowerUpType;
    using it.bomberman.gfx;
    using KeyManager = it.bomberman.input.KeyManager;

    public class Player : AbstractEntity
    {

        public static readonly long DEFAULT_DROP_COOL_DOWN = (long)5e8; // 0.5 s
        public const int DEFAULT_PLAYER_WIDTH = 128;
        public const int DEFAULT_PLAYER_HEIGHT = 100;
        public const int SPEED_MULTIPLIER = 2;
        private Animation animDown, animUp, animLeft, animRight, animBomb;
        private KeyManager keyManager;
        private int playerNumb;
        private Body body;
        private readonly int cropOffsetX = 17;
        private readonly int cropOffsetY = 22;

        private readonly int initialPosX;
        private readonly int initialPosY;
        private int health;
        private int speed;
        private int nBombs;
        private int bombExtension;

        private long spawnTime;
        private bool invulnerable;

        private long lastBombDroppedTime = 0;
        private long bombDroppedCoolDown = DEFAULT_DROP_COOL_DOWN;
        private int xMove;
        private int yMove;
        private ISet<Bomb> bombs;

        protected internal Player(int x, int y, int width, int height, EntityController controller) : base(x, y, width, height, controller)
        {
            this.initialPosX = x;
            this.initialPosY = y;
            init();
        }

        public Player(int x, int y, int n, KeyManager keyManager, EntityController controller) : this(x, y, DEFAULT_PLAYER_WIDTH, DEFAULT_PLAYER_HEIGHT, controller)
        {
            this.playerNumb = n;
            this.keyManager = keyManager;

            this.bombs = new HashSet<Bomb>();

            if (playerNumb == 1)
            {
                animDown = new Animation(150, Assets.player_d);
                animUp = new Animation(150, Assets.player_u);
                animLeft = new Animation(150, Assets.player_l);
                animRight = new Animation(150, Assets.player_r);
                animBomb = new Animation(150, Assets.player_bomb);
            }
            if (playerNumb == 2)
            {
                animDown = new Animation(150, Assets.player_d2);
                animUp = new Animation(150, Assets.player_u2);
                animLeft = new Animation(150, Assets.player_l2);
                animRight = new Animation(150, Assets.player_r2);
                animBomb = new Animation(200, Assets.player_bomb2);
            }
        }

        private void init()
        {
            this.x = initialPosX;
            this.y = initialPosY;
            this.health = 1;
            this.nBombs = 1;
            this.speed = 1;
            this.bombExtension = 1;
            this.invulnerable = true;
            this.spawnTime = System.nanoTime();
        }

        protected internal override void initBody()
        {
            this.body = new Body();
            this.body.add(new Rectangle(this.x + cropOffsetX, this.y + cropOffsetY, 40, 70));
        }

        public virtual void getInput()
        {
            xMove = 0;
            yMove = 0;

            // v(x) = 2x + 2
            int s = SPEED_MULTIPLIER * this.speed + 2;

            if (this.playerNumb == 1)
            {
                if (this.keyManager.up)
                {
                    yMove -= s;
                }
                if (this.keyManager.down)
                {
                    yMove = s;
                }
                if (this.keyManager.left)
                {
                    xMove -= s;
                }
                if (this.keyManager.right)
                {
                    xMove = s;
                }
            }

            if (this.playerNumb == 2)
            {
                if (this.keyManager.up2)
                {
                    yMove -= s;
                }
                if (this.keyManager.down2)
                {
                    yMove = s;
                }
                if (this.keyManager.left2)
                {
                    xMove -= s;
                }
                if (this.keyManager.right2)
                {
                    xMove = s;
                }
            }
            if (this.keyManager.drop && playerNumb == 1)
            {
                dropBomb();
            }

            if (this.keyManager.drop2 && playerNumb == 2)
            {
                dropBomb();
            }
        }

        public override void tick()
        {
            if (System.nanoTime() - this.spawnTime > 2e9)
            {
                this.invulnerable = false;
            }

            this.bombs.removeIf(Bomb.hasFinished);
            animDown.tick();
            animLeft.tick();
            animRight.tick();
            animUp.tick();
            animBomb.tick();

            Input;
            int oldX = this.x;
            int oldY = this.y;
            this.x += xMove;
            this.y += yMove;
            this.body.move(this.x + cropOffsetX, this.y + cropOffsetY);
            if (this.controller.verifyCollision(this))
            {
                this.x = oldX;
                this.y = oldY;
                this.xMove = 0;
                this.yMove = 0;
                this.body.move(this.x + cropOffsetX, this.y + cropOffsetY);
            }
        }

        public void render(Graphics g)
        {
            g.drawImage(CurrentAnimationFrame, x, y, width, height, null);
        }

        private BufferedImage CurrentAnimationFrame()
        {
            if (this.keyManager.drop && playerNumb == 1 && canDropBomb())
            {
                return animBomb.CurrentFrame;
            }
            if (this.keyManager.drop2 && playerNumb == 2 && canDropBomb())
            {
                return animBomb.CurrentFrame;
            }

            if (xMove < 0)
            {
                return animLeft.CurrentFrame;
            }
            else if (xMove > 0)
            {
                return animRight.CurrentFrame;
            }
            else if (yMove < 0)
            {
                return animUp.CurrentFrame;
            }
            else
            {
                return animDown.CurrentFrame;
            }
        }

        public Vector2 Position()
        {
            return new Vector2(this.x, this.y);
        }

        public Body Body()
        {
            // versione non modificabile per preservare l'incapsulamento
            return this.body;
        }

        public bool shouldCollide(ICollidable collidable)
        {
            if (collidable is Wall)
            {
                return true;
            }
            return false;
        }

        public void collision(ICollidable collidable)
        {
            if (collidable is Explosion)
            {
                this.collision((Explosion)collidable);
            }
            if (collidable is PowerUp)
            {
                collision((PowerUp)collidable);
            }
        }

        public void collision(Explosion exp)
        {
            if (!this.invulnerable)
            {
                hit();
            }
        }

        public void collision(Wall wall)
        {
            // Do Nothing
            // Die if deathWall
        }

        public void collision(PowerUp up)
        {
            if (up.Type == PowerUpType.BOMB_NUM)
            {
                this.nBombs += up.Value;
            }

            if (up.Type == PowerUpType.LIFE)
            {
                this.health += up.Value;
            }

            if (up.Type == PowerUpType.BOMB_EXTENSION)
            {
                this.bombExtension += up.Value;
            }

            if (up.Type == PowerUpType.SPEED)
            {
                this.speed += up.Value;
            }
        }

        public void dropBomb()
        {
            if (canDropBomb())
            {
                Bomb b = new Bomb(this.x + cropOffsetX, this.y + cropOffsetY, this.bombExtension, this.controller);
                this.bombs.add(b);
                this.controller.register(b);
                this.lastBombDroppedTime = System.nanoTime();
            }
        }

        public void dispose()
        {
            this.controller.notifyDisposal(this);
        }

        private void die()
        {
            if (this.health >= 1)
            {
                respawn();
            }
            else
            {
                this.dispose();
            }
        }

        private bool canDropBomb()
        {
            return (this.bombs.size() == 0) || ((this.bombs.size() < this.nBombs) && (System.nanoTime() - this.lastBombDroppedTime > this.bombDroppedCoolDown));
        }

        private void hit()
        {
            this.health--;
            die();
        }

        private void respawn()
        {
            int tmp = this.Health;
            init();
            this.health = tmp;
        }

        public int Health()
        {
            return this.health;
        }

        public int Speed()
        {
            return this.speed;
        }

        public int BombsNumber()
        {
            return nBombs;
        }

        public int BombExtension()
        {
            return this.bombExtension;
        }

        public int PlayerNumb()
        {
            return this.playerNumb;
        }

    }
}
