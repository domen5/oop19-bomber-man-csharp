﻿using System.Collections.Generic;

namespace CSharpTask
{
    public class CollisionManager : ICollisionManager
    {
        private readonly List<ICollidable> collidables;

        public CollisionManager()
        {
            this.collidables = new List<ICollidable>();
        }

        public void Register(ICollidable collidable)
        {
            this.collidables.Add(collidable);
        }

        public bool VerifyCollision(ICollidable collidable)
        {
            foreach (ICollidable c in this.collidables)
            {
                // se è interessante la collisione tra collidable ed il c corrente
                // AND i due ICollidable entrano effettivamente in contatto
                if (collidable.ShouldCollide(c) && collidable.GetBody().Intersects(c.GetBody()))
                {
                    return true;
                }
            }
            return false;
        }

        public void VerifyAndResolveCollision(ICollidable collidable)
        {
            foreach (ICollidable other in this.collidables)
            {
                // se è interessante la collisione tra collidable ed il c corrente
                // AND i due ICollidable entrano effettivamente in contatto
                if (collidable.ShouldCollide(other) && collidable.GetBody().Intersects(other.GetBody()))
                {
                    // notifica tutti gli oggetti interessati
                    other.Collision(collidable);
                }
            }
        }
    }
}
