﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpTask
{
    public class Rectangle
    {
        private readonly List<Vector2> vertices;
        private readonly int width;
        private readonly int heigth;
        private readonly int TOP_LEFT_INDEX = 0;
        private readonly int BOTTOM_RIGHT_INDEX = 2;

        /// <summary>
        /// </summary>
        /// <param name="x">Componente x dell'oringine del Rectangle (Veritice in alto a sinistra)</param>
        /// <param name="y">Componente y dell'oringine del Rectangle (Veritice in alto a sinistra)</param>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        public Rectangle(int x, int y, int width, int heigth)
        {
            this.vertices = new List<Vector2>();
            SetPoints(x, y, width, heigth);
            this.width = width;
            this.heigth = heigth;
        }

        /// <summary>
        /// Utilizzato internamene per inizializzare i vertici o per spostare il Rectangle.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="heigth"></param>
        private void SetPoints(int x, int y, int width, int heigth)
        {
            if (this.vertices.Count > 0)
            {
                this.vertices.Clear();
            }
            this.vertices.Add(new Vector2(x, y));
            this.vertices.Add(new Vector2(x + width, y));
            this.vertices.Add(new Vector2(x + width, y + heigth));
            this.vertices.Add(new Vector2(x, y + heigth));
        }

        /// <summary>
        /// Verifica se questo Rectangle e other abbiano dei punti di intersezione
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Intersects(Rectangle other)
        {
            if (this.BottomRight.X <= other.TopLeft.X || this.TopLeft.X >= other.BottomRight.X)
            {
                return false;
            }
            if (this.BottomRight.Y <= other.TopLeft.Y || this.TopLeft.Y >= other.BottomRight.Y)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Restituisce un riferimento al vertice in alto a sinistra del Rectangle.
        /// Corrisponde con l'origine da cui viene calcolata la posizione.
        /// </summary>
        public Vector2 TopLeft
        {
            get { return this.vertices[TOP_LEFT_INDEX]; }
        }

        /// <summary>
        /// Restituisce un riferimento al vertice in basso a destra del Rectangle.
        /// Insieme al TopLeft permette di calcolare width e heigth del Rectangle.
        /// </summary>
        public Vector2 BottomRight
        {
            get { return this.vertices[BOTTOM_RIGHT_INDEX]; }
        }

        /// <summary>
        /// Sposta il Rectangle nella posizione (x,y)
        /// </summary>
        /// <param name="x">Componente x della nuova origine del Rectangle</param>
        /// <param name="y">Componente y della nuova origine del Rectangle</param>
        public void Move(int x, int y)
        {
            SetPoints(x, y, this.width, this.heigth);
        }

        /// <summary>
        /// Classe utilizzata per esprimere un punto in uno spazio intero.
        /// </summary>
        public class Vector2
        {
            private readonly int x;
            private readonly int y;

            public Vector2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public int X
            {
                get { return x; }
            }

            public int Y
            {
                get { return y; }
            }
        }

    }
}
