﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpTask
{
    public interface ICollisionManager
    {
        /// <summary>
        /// Agiiunge un oggetto alla collezione interna
        /// </summary>
        /// <param name="collidable"></param>
        public void Register(ICollidable collidable);

        /// <summary>
        /// Verifica se il parametro entra in contatto con altri ICollidable e risolve le conseguenze
        /// della collisionne
        /// </summary>
        /// <param name="collidable"></param>
        public void VerifyAndResolveCollision(ICollidable collidable);

        /// <summary>
        /// Verifica se il parametro entra in collisione con un altro ICollidable ma non notifica nessun
        /// altro oggetto
        /// </summary>
        /// <param name="collidable"></param>
        /// <returns></returns>
        public bool VerifyCollision(ICollidable collidable);
    }
}
