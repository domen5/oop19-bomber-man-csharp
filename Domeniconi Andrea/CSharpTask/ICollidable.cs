﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace CSharpTask
{
    public interface ICollidable
    {
        /// <summary>
        /// Ritorna vero se il parametro è di un Tipo Concreto con il quale la collisione deve essere intercettata
        /// </summary>
        /// <param name="collidable"></param>
        /// <returns></returns>
        public bool ShouldCollide(ICollidable collidable);

        /// <summary>
        /// Gestisce le conseguenze di una collisione con il parametro
        /// </summary>
        /// <param name="collidable"></param>
        /// <returns></returns>
        public void Collision(ICollidable collidable);

        /// <summary>
        /// Getter per il Rectangle
        /// </summary>
        /// <returns></returns>
        public Rectangle GetBody();
    }
}
