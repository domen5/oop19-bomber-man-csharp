﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpTask
{

    /// <summary>
    /// Lo scopo di questo Task C# è fornire una demo del funzionamento della classe CollisionManager.
    /// Per questo motivo la realizzazione delle classi presenti in Entity.cs è ridotta al minimo indispensabile.
    /// </summary>
    public abstract class Entity : ICollidable
    {
        private readonly Rectangle body;
        protected bool alive = true;

        public Entity(int x, int y, int width, int heigth)
        {
            this.body = new Rectangle(x, y, width, heigth);
        }

        public Rectangle GetBody()
        {
            return this.body;
        }

        public bool IsAlive
        {
            get { return this.alive; }
        }

        public abstract void Collision(ICollidable collidable);
        public abstract bool ShouldCollide(ICollidable collidable);
    }

    public class Explosion : Entity
    {
        public Explosion(int x, int y, int width, int heigth) : base(x, y, width, heigth) { }

        public override bool ShouldCollide(ICollidable collidable)
        {
            return (collidable is Player) || (collidable is HardWall) || (collidable is SimpleWall);
        }

        public override void Collision(ICollidable collidable)
        {
            if(collidable is Player)
            {
                (collidable as Player).Collision(this);
            }
        }
    }
    public class Player : Entity
    {
        private bool canMove;
        public bool CanMove { get { return this.canMove; } }

        public Player(int x, int y, int width, int heigth) : base(x, y, width, heigth) {
            this.canMove = true;
        }

        public override bool ShouldCollide(ICollidable collidable)
        {
            return (collidable is Explosion) || (collidable is HardWall) || (collidable is SimpleWall);
        }

        public override void Collision(ICollidable collidable)
        {
            if(collidable is Explosion)
            {
                this.alive = false;
            }

            if((collidable is HardWall) || (collidable is SimpleWall))
            {
                this.canMove = false;
            }
        }

        public void Move(int x, int y)
        {
            if (CanMove)
            {
                this.GetBody().Move(x, y);
            }
        }
    }

    public class HardWall : Entity
    {
        public HardWall(int x, int y, int width, int heigth) : base(x, y, width, heigth) { }

        public override bool ShouldCollide(ICollidable collidable)
        {
            return false;
        }

        public override void Collision(ICollidable collidable)
        {
            
        }
    }
    public class SimpleWall : Entity
    {
        public SimpleWall(int x, int y, int width, int heigth) : base(x, y, width, heigth) { }

        public override bool ShouldCollide(ICollidable collidable)
        {
            return (collidable is Explosion) || (collidable is Player);
        }

        public override void Collision(ICollidable collidable)
        {
            if (collidable is Explosion)
            {
                this.alive = false;
            }

            if(collidable is Player)
            {
                (collidable as Player).Collision(this);
            }
        }
    }
}
