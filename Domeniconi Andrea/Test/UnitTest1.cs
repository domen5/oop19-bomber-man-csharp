using Microsoft.VisualStudio.TestTools.UnitTesting;
using CSharpTask;

namespace Test
{
    /// <summary>
    /// Lo scopo di questo Task C# � fornire una demo del funzionamento della classe CollisionManager.
    /// Per questo motivo la realizzazione delle classi presenti in Entity.cs � ridotta al minimo indispensabile.
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Test senza movimento. Verifica che alcuni oggetti non risultino pi� Alive dopo
        /// la collisione con Explosion.
        /// </summary>
        [TestMethod]
        public void ExplosionCollisionTest()
        {
            int x = 0, y = 0, playerWidth = 50, explosionWidth = 100, explosionHeigth = 100;

            Player p1 = new Player(x, y, playerWidth, playerWidth);
            SimpleWall simpleWall = new SimpleWall(x + playerWidth + 10, y, playerWidth, playerWidth);
            HardWall hardWall = new HardWall(x + playerWidth + 10, y + playerWidth + 10, playerWidth, playerWidth);

            Explosion e1 = new Explosion(x, y, explosionWidth, explosionHeigth);
            ICollisionManager collisionManager = new CollisionManager();

            collisionManager.Register(p1);
            collisionManager.Register(simpleWall);
            collisionManager.Register(hardWall);

            // all'inizio devono essere tutti vivi
            Assert.IsTrue(p1.IsAlive);
            Assert.IsTrue(simpleWall.IsAlive);
            Assert.IsTrue(hardWall.IsAlive);

            collisionManager.VerifyAndResolveCollision(e1);

            // dopo una collisione con una Explosion solo HardWall dovrebbe risultare vivo
            Assert.IsFalse(p1.IsAlive);
            Assert.IsFalse(simpleWall.IsAlive);
            Assert.IsTrue(hardWall.IsAlive);
        }

        /// <summary>
        /// Test che verifica la corretta gestione delle collisioni anche in movimento.
        /// Verifica che Player.CanMove risulti false dopo la collisione con SimpleWall.
        /// </summary>
        [TestMethod]
        public void PlayerMovementTest()
        {
            int playerX = 0, playerY = 0, width = 50, wallX = 100, wallY = 0;
            int gap = 1 + wallX - playerX - width;

            Player p1 = new Player(playerX, playerY, width, width);
            SimpleWall wall = new SimpleWall(wallX, wallY, width, width);
            ICollisionManager collisionManager = new CollisionManager();
            collisionManager.Register(p1);
            collisionManager.Register(wall);

            for (int i = 1; i <= gap; i++)
            {
                Assert.IsTrue(p1.CanMove);
                p1.Move(i, playerY);
                collisionManager.VerifyAndResolveCollision(p1);
            }
            Assert.IsFalse(p1.CanMove);
        }

        /// <summary>
        /// Questo test verifica la corretta gestione delle collisioni tra Player e Explosion.
        /// Inoltre � diverso dal primo perch� la collisione non � innescata da Explosion ma da Player.
        /// </summary>
        [TestMethod]
        public void PlayerAndExplosionDynamicTest()
        {
            int playerX = 0, playerY = 0, width = 50, explX = 100, explY = 0;
            int gap = 1 + explX - playerX - width;

            Player p1 = new Player(playerX, playerY, width, width);
            Explosion expl = new Explosion(explX, explY, width, width);
            ICollisionManager collisionManager = new CollisionManager();
            collisionManager.Register(p1);
            collisionManager.Register(expl);

            for (int i = 1; i <= gap; i++)
            {
                Assert.IsTrue(p1.IsAlive);
                p1.Move(i, playerY);
                collisionManager.VerifyAndResolveCollision(p1);
            }
            Assert.IsFalse(p1.IsAlive);
        }
    }
}
