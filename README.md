# README #

### What is this repository for? ###
Repository per i task C# del progetto OOP19-bomber-man di Dall'Ara Emanuele e Domeniconi Andrea (ex Christian Daniel Stratu)

* BomberMan C# Task
* Version 1.0
* Author: Dall'Ara Emanuele - Domeniconi Andrea

### Domeniconi Andrea ###

Questa cartella contiene la traduzione in C# della classi CollsionManager e Rectangle responsabili 
della gestione delle collisioni rispettivamente a livello logico e geometrico.
Al fine di presentare un prototipo funzionante sotto forma di UnitTest sono state implementate anche
le classi Player, SimpleWall, HardWall ed Explosion (presenti nel file Entity.cs) anche se in maniera
minima e rundimentale.

### Dall'Ara Emanuele ###

Questa cartella contiene la traduzione in C# della classi Clock e Player responsabili 
della gestione del timer di gioco e dell'oggetto player e dei suoi listener.
Presente anche l'interfaccia ArenaModel che definisce i metodi per controllare e disegnare sull'arena i vari sprite e controlli.